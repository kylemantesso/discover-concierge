import React, {useEffect, useState} from 'react';
import './App.css';
import {Box, Button, Heading, Label, Text, TextField} from "gestalt";
import * as firebase from "firebase";

// hardcoded user
const DATABSE_REF = "questions/1";

function App() {

  const [data, setData] = useState('');

  useEffect(() => {
    firebase.database().ref(DATABSE_REF).once('value').then((snapshot) => {
      setData(snapshot.val());
    });
  }, []);

  return (
    <div className="app">
      <header>
        <img style={{height: 80}} src="banner.png" />
      </header>
      <div className="app-body">
        <Heading size="xs">Airbnb Host</Heading>
        <Box height={20} />
        <Heading>Customise</Heading>
        <Text>Help your guests with these frequently asked questions</Text>
        <Box height={40} ></Box>
        <Box >
          <Box marginBottom={2}>
            <Label htmlFor="email">
              <Text>How far is away is the city center?</Text>
            </Label>
          </Box>
          <TextField
            id="city"
            onChange={(e) => { setData({...data, CityCenterDistance: e.value || ""})}}
            value={data.CityCenterDistance}
          />
        </Box>
        <Box height={40} ></Box>
        <Box >
          <Box marginBottom={2}>
            <Label htmlFor="email">
              <Text>Where can I park?</Text>
            </Label>
          </Box>
          <TextField
            id="parking"
            onChange={(e) => { setData({...data, ParkingDetails: e.value || ""})}}
            value={data.ParkingDetails}
          />
        </Box>
        <Box height={40} ></Box>
        <Box >
          <Box marginBottom={2}>
            <Label htmlFor="email">
              <Text>What are the Wifi details?</Text>
            </Label>
          </Box>
          <TextField
            id="parking"
            onChange={(e) => { setData({...data, WifiDetails: e.value || ""})}}
            value={data.WifiDetails}
          />
        </Box>
        <Box height={40} />
        <Box width={200}  >
          <Button text={"Save"} onClick={() => {
            firebase.database().ref(DATABSE_REF).set(data);
          }}/>

        </Box>
      </div>
    </div>
  );
}

export default App;
