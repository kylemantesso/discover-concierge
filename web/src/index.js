import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'gestalt/dist/gestalt.css';
import * as firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyBEt1l2Qm7qGTSRbsEdvaXnA0xXTHaEdn8",
  databaseURL: "https://discover-concierge.firebaseio.com",
  projectId: "discover-concierge",
  appId: "1:100863253001:web:67312ca6f627ea1d26f6d8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
