import { WebhookClient } from 'dialogflow-fulfillment';
import {Request, Response} from "firebase-functions";
import {Intents} from "../intents/intents";
import {getCustomQuestion} from "../intents/getCustomQuestion";
import {getMerchants} from "../intents/getMerchants";
import {getTipping} from "../intents/getTipping";

export const fulfillmentFn = (request: Request, response: Response) => {
  console.log('fulfillmentFn:request', request);

  const agent = new WebhookClient({ request, response });

  // @ts-ignore
  let intentMap: Map<string, (agent: any) => Promise<any | void>> = new Map();
  intentMap.set(Intents.CityCenterDistance, getCustomQuestion);
  intentMap.set(Intents.WifiDetails, getCustomQuestion);
  intentMap.set(Intents.ParkingDetails, getCustomQuestion);

  intentMap.set(Intents.Merchants, getMerchants);
  intentMap.set(Intents.Tipping, getTipping);

  agent.handleRequest(intentMap);
};
