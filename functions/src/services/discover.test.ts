import { discoverClient } from "./discover";

// @ts-ignore
const run = async () => {
  let tips;
  try {
    tips = await discoverClient.getMerchants();
  } catch (e) {
    console.log(e);
  }
  // @ts-ignore
  console.log("TIPS", tips.result[5]);
};

run();
