import axios, {AxiosRequestConfig} from 'axios';

const API_KEY = "AIzaSyCqxNzEAiUJ5hPb1nT9PfiC_P4begBF7UQ";

export const getPhotoUrl = (ref: string) => {
  const url = `https://maps.googleapis.com/maps/api/place/photo?maxheight=400&photoreference=${ref}&key=${API_KEY}`;
  console.log('GOOGLE PHOTO URL', url);
  return url;
}

// @ts-ignore
export const getPlaceDetails = async (placeId: string) => {
  const url =  `https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&key=${API_KEY}`;

  const config: AxiosRequestConfig = {
    method: 'GET',
    url,
  };

  try {
    const res = await axios(config);
    console.log(res);
    // @ts-ignore
    console.log(Object.keys(res));
    return res;
  } catch(e) {
    console.log(e);
  }

}