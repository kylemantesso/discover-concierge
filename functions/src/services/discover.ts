import axios, {AxiosRequestConfig, Method} from 'axios';

const API_SERVER = 'https://apis.discover.com';

class DiscoverAPI {
  id: string;
  secret: string;
  server: string;

  tokenExpiry?: number;
  token?: {
    access_token: string;
    expires_in: number;
    token_type: string;
  };

  constructor(id: string, secret: string, server?: string) {
    this.id = id;
    this.secret = secret;
    this.server = server || API_SERVER;
  }

  // @ts-ignore
  async request(scope: string, plan: string, url: string, method: Method, data?: string) {
      await this.refreshToken(scope);


    const config: AxiosRequestConfig = {
      method,
      baseURL: API_SERVER,
      url,
      data,
      headers: {
        'Content-Type': 'application/json',
        'X-DFS-API-PLAN': plan,
        'Accept': 'application/json',
        'Context-Type': 'application/json',
        Authorization: `Bearer ${this.token!.access_token}`
      }
    };

    try {
      const res = await axios(config);
      // @ts-ignore
      return res.data;
    } catch (e) {
      console.log(e);
    }
  }

  // @ts-ignore
  async refreshToken(scope: string) {
    const config: AxiosRequestConfig = {
      method: 'post',
      baseURL: API_SERVER,
      url: '/auth/oauth/v2/token',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Cache-Control': 'no-cache' },
      data:
        `grant_type=client_credentials&scope=${scope}`,
      auth: {
        username: this.id,
        password: this.secret
      }
    };

    try {
      const res = await axios(config);

      // @ts-ignore
      this.token = res.data;
      // @ts-ignore
      this.tokenExpiry = Date.now() + (this.token!.expires_in - 300);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  getMerchants(city: string = "London") {
    return this.request('CITYGUIDES', 'CITYGUIDES_SANDBOX', `/cityguides/v2/merchants?card_network=DCI&merchant_city=${city}&merchant_category=restaurants`, 'GET');
  }

  getTipGuide(language: string = 'en', countryISOCode: string = '234') {
    return this.request('DCI_TIP', 'DCI_TIPETIQUETTE_SANDBOX', `/dci/tip/v1/guide?languagecd=${language}&countryisonum=${countryISOCode}`, 'GET');
  }

  // getExchangeRate(country: string) {
  //   return this.request('DCI_CURRENCYCONVERSION_SANDBOX', `/dci/currencyconversion/v1/exchangerate?currencycd=${country}`, "GET")
  // }
}

export const discoverClient = new DiscoverAPI(
  "l7xx1a1ad75ec74d4a219d101d646dda41e0",
  "b15d253447a342c68a16889b339e5e3a"
);
