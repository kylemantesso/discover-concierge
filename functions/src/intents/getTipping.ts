import {discoverClient} from "../services/discover";
import {WebhookClient} from "dialogflow-fulfillment";


const UNITED_KINGDOM = '234';

// @ts-ignore
export const getTipping = async (agent: WebhookClient) => {
  const tipping: any = await discoverClient.getTipGuide('en', UNITED_KINGDOM);

  // TODO: Implement other types of tipping - only taxi tipping for now in the UK
  const taxiTip = tipping.find((tipInfo: any) => tipInfo.tipCategoryDesc === "Taxi");

  // Taxi roundup tip
  if(taxiTip.tipCategoryId === 3) {
    agent.add(`Yes, it is polite to round up your taxi fare while in the United Kingdom`);
  }

};