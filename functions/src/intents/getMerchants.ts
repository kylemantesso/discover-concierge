import {discoverClient} from "../services/discover";
import {WebhookClient, Card} from "dialogflow-fulfillment";
import {getPhotoUrl, getPlaceDetails} from "../services/google";


// @ts-ignore
export const getMerchants = async (agent: WebhookClient) => {
  const merchants: any = await discoverClient.getMerchants();
  const merchant = merchants.result[5];


  console.log("MERCHANT", merchant);

  console.log("MERCHANT GOOGLE ID", merchant.google_place_id);

  if(merchant.google_place_id) {
    console.log('MERCHANT HAS PLACE ID', merchant.google_place_id)
    const googleDetails: any = await getPlaceDetails(merchant.google_place_id);
    console.log('MERCHANT HAS GOOGLED DETAILS', googleDetails.data.result)

      const cardResponse = new Card({
        title: `${merchant.name}`,
        imageUrl: getPhotoUrl(googleDetails.data.result.photos[0].photo_reference),
        text: googleDetails.data.result.formatted_address,
      });

      agent.add(cardResponse);

      agent.add(`${merchant.name} is a great restaurant just down the road at ${merchant.address1}. It has a rating of ${googleDetails.data.result.rating}`)

  } else {

  agent.add(`${merchant.name} is a great restaurant just down the road at ${merchant.address1}`);
  }
};