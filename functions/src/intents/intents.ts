export enum Intents {
  CityCenterDistance = 'CityCenterDistance',
  WifiDetails = 'WifiDetails',
  ParkingDetails = 'ParkingDetails',
  Merchants = "Merchants",
  Tipping = "Tipping"
}
