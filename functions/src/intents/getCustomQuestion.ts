import {WebhookClient} from "dialogflow-fulfillment";
import {firebase} from "../services/firebase";


// hardcoded user id for now, this would change with authentication
const USER_ID = 1;

// @ts-ignore
export const getCustomQuestion = async (agent: WebhookClient) => {
  console.log('Intent', agent.intent);

  const ref = firebase.database().ref('questions/'+USER_ID);
  const data = await ref.once('value').then((snapshot: any) => snapshot.val());

  const response = data[agent.intent];

  console.log('Response', response);

  agent.add(response);

};
