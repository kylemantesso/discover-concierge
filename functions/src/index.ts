import * as functions from 'firebase-functions';
import * as _cors from 'cors';
import {fulfillmentFn} from "./functions/fulfillment";

const options: _cors.CorsOptions = {
  origin: '**',
};
const cors = _cors;

export const fulfillment = functions.https.onRequest((request, response) => {
  cors(options)(request, response, () => fulfillmentFn(request, response));
});
